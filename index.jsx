import React from 'react'
import {
  Renderer,
  Window,
  View,
  Text,
} from '@nodegui/react-nodegui'
import {
  WidgetEventTypes,
} from '@nodegui/nodegui'
import {exec} from 'child_process'

const play = (file) => exec(`aplay ${file}`)
const rand = (max) => Math.floor((Math.random() * max) + 1)
const playRand = (name, max = 3) => {console.log(`${name}-${rand(max)}.wav`);play(`${name}-${rand(max)}.wav`)}

const App = () => {
  const window = React.useRef()

  React.useEffect(() => {
    if (!window.current) return

    window.current.resize(720, 480)
  }, [window])

  return pug`
    Window(
      ref=window
      windowTitle='Hello world'
      styleSheet=styleSheet
      on={
        [WidgetEventTypes.KeyPress]: ()=>playRand('sounds/down'),
        [WidgetEventTypes.KeyRelease]: ()=>playRand('sounds/up'),
      }
    )
      View#root
        View#header
          Text < Back
          Text Lesson 1 of 12
          Text $

        View#container
          Text#title Hello <React node='gui' />
          Text#body
            | Here is a tool that does what we need,
            | and it may even help us move quicker!
  `
}

const styleSheet = `
  * {
    color: #ccc;
  }

  #root {
    flex: 1;
    flex-direction: column;
    background-color: #222;
  }

  #header {
    height: 40px;
    background-color: #333;
    border-bottom: 1px solid #222;
    flex: 0;
    flex-direction: row;
    justify-content: center;
  }

  #container {
    flex: 1;
    flex-direction: column;
    padding: 24px;
  }

  #title {
    font-size: 42px;
    font-weight: bold;
    color: #bbb;
  }

  #body {
    font-size: 24px;
  }
`

Renderer.render(<App/>)
